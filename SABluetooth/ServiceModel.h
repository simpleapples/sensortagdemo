//
//  ServiceModel.h
//  SABluetooth
//
//  Created by Zzy on 1/17/15.
//  Copyright (c) 2015 Zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ServiceModel : NSObject

@property (copy, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) CBService *service;
@property (strong, nonatomic, readonly) NSString *dataUUID;
@property (copy, nonatomic) NSString *value;

- (instancetype)initWithService:(CBService *)service value:(NSString *)value;

@end
