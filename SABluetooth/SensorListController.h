//
//  SensorListController.h
//  SABluetooth
//
//  Created by Zzy on 1/16/15.
//  Copyright (c) 2015 Zzy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface SensorListController : UITableViewController

@property (strong, nonatomic) CBPeripheral *peripheral;

@end
