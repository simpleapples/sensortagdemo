//
//  SensorListController.m
//  SABluetooth
//
//  Created by Zzy on 1/16/15.
//  Copyright (c) 2015 Zzy. All rights reserved.
//

#import "SensorListController.h"
#import "ServiceModel.h"
#import "SensorsUtils.h"

@interface SensorListController () <CBPeripheralDelegate>

@property (strong, nonatomic) NSMutableArray *sensorList;
@property (strong, nonatomic) NSTimer *refreshTimer;

@end

@implementation SensorListController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sensorList = [[NSMutableArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.sensorList removeAllObjects];
    self.peripheral.delegate = self;
    [self.peripheral discoverServices:nil];
    
    self.refreshTimer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(refreshValue) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.refreshTimer forMode:NSRunLoopCommonModes];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.refreshTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)refreshValue
{
    [self.tableView reloadData];
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    [peripheral.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CBService *service = obj;
        ServiceModel *serviceModel = [[ServiceModel alloc] initWithService:service value:@"N/A"];
        if (serviceModel.name) {
            [peripheral discoverCharacteristics:nil forService:service];
            [self.sensorList addObject:serviceModel];
        }
    }];
    [self.tableView reloadData];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    [service.characteristics enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CBCharacteristic *ch = obj;
        NSString *seventhChar = [ch.UUID.UUIDString substringWithRange:NSMakeRange(7, 1)];
        if ([seventhChar isEqualToString:@"2"]) {
            uint8_t data = 0x01;
            [peripheral writeValue:[NSData dataWithBytes:&data length:1] forCharacteristic:ch type:CBCharacteristicWriteWithResponse];
        } else if ([seventhChar isEqualToString:@"1"]) {
            [peripheral setNotifyValue:YES forCharacteristic:ch];
        }
    }];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    [self.sensorList enumerateObjectsUsingBlock:^(ServiceModel *item, NSUInteger idx, BOOL *stop) {
        if ([characteristic.UUID.UUIDString isEqualToString:item.dataUUID]) {
            if ([item.name isEqualToString:@"IR temperature"]) {
                CGFloat irtemperature = [sensorTMP006 calcTObj:characteristic.value];
                CGFloat temperature = [sensorTMP006 calcTAmb:characteristic.value];
                item.value = [NSString stringWithFormat:@"IR: %.2f TEMP: %.2f", irtemperature, temperature];
            }
        }
    }];
}

#pragma mark - UITbaleViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sensorList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceModel *serviceModel = [self.sensorList objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    cell.textLabel.text = serviceModel.name;
    cell.detailTextLabel.text = serviceModel.value;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
