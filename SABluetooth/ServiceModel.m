//
//  ServiceModel.m
//  SABluetooth
//
//  Created by Zzy on 1/17/15.
//  Copyright (c) 2015 Zzy. All rights reserved.
//

#import "ServiceModel.h"

@interface ServiceModel ()

@property (copy, nonatomic, readwrite) NSString *name;
@property (strong, nonatomic, readwrite) CBService *service;

@end

@implementation ServiceModel

- (instancetype)initWithService:(CBService *)service value:(NSString *)value
{
    self = [super init];
    if (self) {
        _service = service;
        _value = value;
    }
    return self;
}

- (NSString *)name
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:@"IR temperature" forKey:@"F000AA00-0451-4000-B000-000000000000"];
    [dictionary setValue:@"Accelerometer" forKey:@"F000AA10-0451-4000-B000-000000000000"];
    [dictionary setValue:@"Humidity" forKey:@"F000AA20-0451-4000-B000-000000000000"];
    [dictionary setValue:@"Magnetometer" forKey:@"F000AA30-0451-4000-B000-000000000000"];
    [dictionary setValue:@"Barometer" forKey:@"F000AA40-0451-4000-B000-000000000000"];
    [dictionary setValue:@"Gyroscope" forKey:@"F000AA50-0451-4000-B000-000000000000"];
    
    return [dictionary objectForKey:self.service.UUID.UUIDString];
}

- (NSString *)dataUUID
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:@"F000AA01-0451-4000-B000-000000000000" forKey:@"IR temperature" ];
    [dictionary setValue:@"F000AA11-0451-4000-B000-000000000000" forKey:@"Accelerometer"];
    [dictionary setValue:@"F000AA21-0451-4000-B000-000000000000" forKey:@"Humidity"];
    [dictionary setValue:@"F000AA31-0451-4000-B000-000000000000" forKey:@"Magnetometer"];
    [dictionary setValue:@"F000AA41-0451-4000-B000-000000000000" forKey:@"Barometer"];
    [dictionary setValue:@"F000AA51-0451-4000-B000-000000000000" forKey:@"Gyroscope"];
    
    return [dictionary objectForKey:self.name];
}

@end
