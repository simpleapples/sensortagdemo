//
//  BluetoothListController.m
//  SABluetooth
//
//  Created by Zzy on 1/16/15.
//  Copyright (c) 2015 Zzy. All rights reserved.
//

#import "BluetoothListController.h"
#import "SensorListController.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface BluetoothListController () <CBCentralManagerDelegate>

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) NSMutableArray *peripheralsList;
@property (assign, nonatomic) NSInteger currentIndex;
@property (assign, nonatomic, getter = isCellRegistered) BOOL cellRegistered;

@end

@implementation BluetoothListController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.peripheralsList = [[NSMutableArray alloc] init];
    
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self refreshDevice];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)refreshDevice
{
    [self.peripheralsList removeAllObjects];
    [self.tableView reloadData];
    if (self.centralManager.state == CBCentralManagerStatePoweredOn) {
        [self.centralManager scanForPeripheralsWithServices:nil options:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[SensorListController class]]) {
        SensorListController *sensorListController = (SensorListController *)segue.destinationViewController;
        sensorListController.peripheral = [self.peripheralsList objectAtIndex:self.currentIndex];
    }
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    [self refreshDevice];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (![self.peripheralsList containsObject:peripheral]) {
        [self.peripheralsList addObject:peripheral];
        [self.tableView reloadData];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [self performSegueWithIdentifier:@"BluetoothListToSensorListSegue" sender:nil];
}

#pragma mark - UITbaleViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.peripheralsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *peripheral = [self.peripheralsList objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    if (!peripheral.name) {
        cell.textLabel.text = @"Unknown";
    } else {
        cell.textLabel.text = peripheral.name;
    }
    cell.detailTextLabel.text = peripheral.identifier.UUIDString;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *peripheral = [self.peripheralsList objectAtIndex:indexPath.row];
    [self.centralManager connectPeripheral:peripheral options:nil];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Handler

- (IBAction)onRefreshClick:(id)sender
{
    [self refreshDevice];
}

@end
